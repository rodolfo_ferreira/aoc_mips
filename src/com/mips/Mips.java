package com.mips;

import java.io.IOException;

import com.mips.util.Input;
import com.mips.util.Output;
import com.mips.util.Util;

/**
 * The Class Mips, represents the whole Mips
 */
public class Mips {

	/** The given input. */
	private Input input;

	/** The output file */
	private Output output;

	/**
	 * Instantiates a new mips.
	 *
	 */
	Mips() {

	}

	public void load(String path) {
		this.input = new Input(path);

	}

	public void setOutput(String path) {
		this.output = new Output(path);
	}

	/**
	 * Execute the mips.
	 *
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception with the input file has
	 *             occurred.
	 */
	public boolean execute() throws IOException {
		boolean result = false;
		String contentHex = input.getContent();
		String contentBin = Util.convertToBin(contentHex);
		String[] contents = contentBin.split("[\n\r\t]+");

		RegisterBank bank = RegisterBank.getInstance();

		String outputString = "";
		// LENDO AS INSTRUÇÕES
		for (int i = bank.getPc() / 4; i < contents.length;) {
			String content = contents[i];
			bank.incrementPc();
			InstructionManager instruction = new InstructionManager(content.toCharArray());
			outputString += instruction.getAsAssembly() + "\n";
			outputString += RegisterBank.getInstance().statusBank() + "\n";
			i = bank.getPc() / 4;
		}
		output.writeContent(outputString);

		return result;
	}
}
