package com.mips.exceptions;

/**
 * The Exception Class to inform that the type represents something but a bit.
 */
public class NotBitException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3270326302668970077L;

	/**
	 * Instantiates a new not bit exception.
	 *
	 * @param message
	 *            the message to be shown
	 */
	public NotBitException(String message) {
		super(message);
	}

}
