package com.mips.exceptions;

/**
 * The Exception Class to inform a Format Not Recognized in some instruction.
 */
public class FormatNotRecognized extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3956391823859704497L;

	/**
	 * Instantiates the new Format Not Recognized exception.
	 *
	 * @param message
	 *            the message to be shown
	 */
	public FormatNotRecognized(String message) {
		super(message);
	}
}
