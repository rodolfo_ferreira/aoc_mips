package com.mips;

import java.util.HashMap;

public class Memory {

	private HashMap<Integer, Object> memory;

	private static Memory instance;

	public static Memory getInstance() {
		if (instance == null)
			instance = new Memory();
		return instance;
	}

	private Memory() {
		this.memory = new HashMap<Integer, Object>();
	}

	public char[] get(int address) {
		char[] result = (char[]) memory.get(address);
		if (result == null) {
			result = new char[8];
			result[0] = '0';
			result[1] = '0';
			result[2] = '0';
			result[3] = '0';
			result[4] = '0';
			result[5] = '0';
			result[6] = '0';
			result[7] = '0';
		}
		return (char[]) result;
	}

	public void put(int address, char[] value) {
		this.memory.put(address, value);
	}
}
