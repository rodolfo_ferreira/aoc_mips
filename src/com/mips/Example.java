package com.mips;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Example {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String inputPath = System.getProperty("user.dir") + "\\input\\entrada.txt";
		String outputPath = System.getProperty("user.dir") + "\\output\\saida.txt";

		if (args[0] != null)
			inputPath = args[0];
		if (args[1] != null)
			outputPath = args[1];
		
		
		Mips mips = new Mips();
		mips.load(inputPath);
		mips.setOutput(outputPath);
		mips.execute();

	}

}
