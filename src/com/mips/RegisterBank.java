package com.mips;

import com.mips.exceptions.FormatNotRecognized;

public class RegisterBank {

	private int registers[];

	private int mflo;

	private int mfhi;

	private int pc;

	private static RegisterBank instance;

	public static RegisterBank getInstance() {
		if (instance == null)
			instance = new RegisterBank();
		return instance;
	}

	private RegisterBank() {
		this.pc = 0;
		this.registers = new int[32];
		for (int i = 0; i < this.registers.length; i++) {
			this.registers[i] = 0;
		}
	}

	public String statusBank() {
		String result = "";
		for (int i = 0; i < registers.length; i++) {
			result += String.format("$%d=%d;", i, registers[i]);
		}
		return result;
	}

	public int getRegValue(int registerNumber) {
		return registers[registerNumber];
	}

	public void setRegValue(int registerNumber, int value) {
		if (registerNumber != 0)
			registers[registerNumber] = value;
	}

	public int getMfhi() {
		return mfhi;
	}

	public int getMflo() {
		return mflo;
	}

	public int getPc() {
		return pc;
	}

	public void setPc(int pc) {
		if (pc % 4 != 0)
			throw new FormatNotRecognized("The PC value is not recognizeds");
		this.pc = pc;
	}

	public void setMfhi(int mfhi) {
		this.mfhi = mfhi;
	}

	public void setMflo(int mflo) {
		this.mflo = mflo;
	}

	public void incrementPc() {
		this.pc += 4;

	}

}
