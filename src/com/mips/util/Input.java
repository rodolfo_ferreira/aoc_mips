package com.mips.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * The Class to represent the Input.
 */
public class Input {

	/** The file to be open. */
	private final File file;

	/**
	 * Instantiates a new kind of input.
	 *
	 * @param path
	 *            the path to the file
	 */
	public Input(String path) {
		this.file = new File(path);
	}

	/**
	 * Gets the content inside the file.
	 *
	 * @return the content of the file
	 * @throws IOException
	 *             Signals that an I/O exception in the input file has occurred.
	 */
	public String getContent() throws IOException {
		String result = "";
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		while (br.ready()) {
			result += br.readLine() + '\n';
		}
		br.close();
		return result;

	}

}
