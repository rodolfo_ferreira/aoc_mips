package com.mips.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Output {
	
	/** The file to be written. */
	private final File file;

	/**
	 * Instantiates a new kind of input.
	 *
	 * @param path
	 *            the path to the file
	 */
	public Output(String path) {
		this.file = new File(path);
	}

	/**
	 * Writes the content inside the file.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception in the input file has occurred.
	 */
	public void writeContent(String content) throws IOException {
		if (!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
		fw.close();

	}

}
