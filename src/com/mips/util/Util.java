package com.mips.util;

import java.math.BigInteger;

import com.mips.exceptions.NotBitException;

/**
 * The Class to help to store Static functions.
 */
public class Util {

	/**
	 * This method transforms a hexadecimal represented as String number into a
	 * Binary represented as String number.
	 *
	 * @param hex,
	 *            the hexadecimal number represented as String
	 * @return the binary result represented as String
	 */
	public static String hexToBin(String hex) {
		return new BigInteger(hex, 16).toString(2);

	}

	/**
	 * Convert a text containing ONLY HEXADECIMAL NUMBERS in binary. The number
	 * has to be divided by line break (\n) or return line (\r).
	 *
	 * @param contentHex
	 *            the text which contains the hexadecimal values
	 * @return the string the converted text in binary.
	 */
	public static String convertToBin(String contentHex) {
		String result = "";
		String[] lines = contentHex.split("[\n]+");
		// ArrayList<String> lines = new ArrayList<String>(hex);
		for (String line : lines) {
			if (line.startsWith("0x")) {
				line = line.substring(2);
			}
			String bin = hexToBin(line);
			while (bin.length() < 32) {
				bin = '0' + bin;
			}
			result += bin + "\n";
		}

		return result;
	}

	/**
	 * This method converts decimal to binary.
	 *
	 * @param the
	 *            decimal number
	 * 
	 * @return bits the array which contains the binary representation
	 */
	public static char[] decToBin(int dec) {
		int[] resInt = new int[32];
		char[] result = new char[32];

		int partial = dec;

		for (int i = 31; i >= 0; i--) {

			if ((partial / 2) >= 2) {
				resInt[i] = partial % 2;
				partial = partial / 2;
			} else {
				resInt[i] = partial % 2;
				resInt[i - 1] = partial / 2;
				break;
			}
		}

		for (int i = 0; i < result.length; i++) {
			result[i] = Character.forDigit(resInt[i], 10);
		}

		return result;
	}

	/**
	 * This method converts decimal to binary.
	 *
	 * @param the
	 *            decimal number
	 * 
	 * @return bits the array which contains the binary representation
	 */
	public static char[] decToBinComp2(int dec) {
		int[] resInt = new int[32];
		char[] preresult = new char[32];

		int partial = dec;
		if (partial < 0) {
			partial *= -1;
		} else {
			return decToBin(dec);
		}

		for (int i = 31; i >= 0; i--) {

			if ((partial / 2) >= 2) {
				resInt[i] = partial % 2;
				partial = partial / 2;
			} else {
				resInt[i] = partial % 2;
				resInt[i - 1] = partial / 2;
				break;
			}
		}

		for (int i = 0; i < preresult.length; i++) {
			preresult[i] = Character.forDigit(resInt[i], 10);
		}

		char[] result = new char[32];
		boolean convert = false;
		for (int i = 31; i >= 0; i--) {
			if (preresult[i] == '0') {
				if (!convert) {
					result[i] = preresult[i];
				} else {
					if (preresult[i] == '0') {
						result[i] = '1';
					} else if (preresult[i] == '1') {
						result[i] = '0';
					}
				}
			} else if (preresult[i] == '1') {
				if (!convert) {
					result[i] = preresult[i];
					convert = true;
				} else {
					if (preresult[i] == '0') {
						result[i] = '1';
					} else if (preresult[i] == '1') {
						result[i] = '0';
					}
				}
			}
		}

		return result;
	}

	/**
	 * This method converts binary to decimal.
	 *
	 * @param bits
	 *            the array which contains the binary representation
	 * @return the decimal number
	 */
	public static int binToDec(char[] bits) {
		int result = 0;
		for (int i = 0; i < bits.length; i++) {
			char currentChar = bits[i];
			if (currentChar != '1' && currentChar != '0') {
				throw new NotBitException("The char array contains not bit representation");
			}
			result += Character.getNumericValue(bits[i]) * Math.pow(2, (bits.length - 1) - i);

		}
		return result;
	}

	/**
	 * This method converts the binary to decimal assuming binary numbers
	 * negatif (Complemento a dois)
	 *
	 * @param bits
	 *            the bits to be converted to decimal
	 * @return the int the decimal representation
	 */
	public static int binToDecComp2(char[] bits) {
		int result = 0;
		if (bits[0] == '0') {
			result = binToDec(bits);
		} else if (bits[0] == '1') {
			char[] newBit = new char[bits.length];
			boolean wasFound = false;
			for (int i = bits.length - 1; i >= 0; i--) {
				if (bits[i] == '0') {
					if (wasFound) {
						newBit[i] = '1';
					} else {
						newBit[i] = '0';
					}
				} else if (bits[i] == '1') {
					if (wasFound) {
						newBit[i] = '0';
					} else {
						wasFound = true;
						newBit[i] = '1';
					}
				}

			}
			result = 0 - binToDec(newBit);
		} else {
			throw new NotBitException("The " + bits[0] + " is not a bit representation");
		}
		return result;
	}

	/**
	 * Method that concats the char array to a string
	 *
	 * @param charArray
	 *            the char array
	 * @return the string the concated string
	 */
	public static String concatChar(char[] charArray) {
		StringBuilder builder = new StringBuilder();
		for (char c : charArray) {
			builder.append(c);
		}
		return builder.toString();
	}

	public static String binToHex(char[] bin) {
		String result = "";
		int j = 0;
		String concat = "";
		for (int i = bin.length - 1; i >= 0; i--) {
			j++;
			concat = bin[i] + concat;
			if (j == 4 || i == 0) {

				switch (concat) {
				case "0000":
					result = "0" + result;
					break;

				case "0001":
					result = "1" + result;
					break;

				case "0010":
					result = "2" + result;
					break;

				case "0011":
					result = "3" + result;
					break;

				case "0100":
					result = "4" + result;
					break;

				case "0101":
					result = "5" + result;
					break;

				case "0110":
					result = "6" + result;
					break;

				case "0111":
					result = "7" + result;
					break;

				case "1000":
					result = "8" + result;
					break;

				case "1001":
					result = "9" + result;
					break;

				case "1010":
					result = "a" + result;
					break;

				case "1011":
					result = "b" + result;
					break;

				case "1100":
					result = "c" + result;
					break;

				case "1101":
					result = "d" + result;
					break;

				case "1110":
					result = "e" + result;
					break;

				case "1111":
					result = "f" + result;
					break;

				}
				concat = "";
				j = 0;
			}
		}
		while (result.length() < 8) {
			result = result.charAt(0) + result;
		}
		return result;
	}

}
