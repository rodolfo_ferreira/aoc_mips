package com.mips;

import java.util.Arrays;

import com.mips.exceptions.FormatNotRecognized;
import com.mips.instructions.Instruction;
import com.mips.instructions.InstructionI;
import com.mips.instructions.InstructionJ;
import com.mips.instructions.InstructionR;
import com.mips.instructions.typei.Addi;
import com.mips.instructions.typei.Addiu;
import com.mips.instructions.typei.Andi;
import com.mips.instructions.typei.Beq;
import com.mips.instructions.typei.Bltz;
import com.mips.instructions.typei.Bne;
import com.mips.instructions.typei.Lb;
import com.mips.instructions.typei.Lbu;
import com.mips.instructions.typei.Lui;
import com.mips.instructions.typei.Lw;
import com.mips.instructions.typei.Ori;
import com.mips.instructions.typei.Sb;
import com.mips.instructions.typei.Slti;
import com.mips.instructions.typei.Sltiu;
import com.mips.instructions.typei.Sw;
import com.mips.instructions.typei.Xori;
import com.mips.instructions.typej.J;
import com.mips.instructions.typej.Jal;
import com.mips.instructions.typer.Add;
import com.mips.instructions.typer.Addu;
import com.mips.instructions.typer.And;
import com.mips.instructions.typer.Div;
import com.mips.instructions.typer.Divu;
import com.mips.instructions.typer.Jr;
import com.mips.instructions.typer.Mfhi;
import com.mips.instructions.typer.Mflo;
import com.mips.instructions.typer.Mult;
import com.mips.instructions.typer.Multu;
import com.mips.instructions.typer.Nor;
import com.mips.instructions.typer.Or;
import com.mips.instructions.typer.Sll;
import com.mips.instructions.typer.Sllv;
import com.mips.instructions.typer.Slt;
import com.mips.instructions.typer.Sltu;
import com.mips.instructions.typer.Sra;
import com.mips.instructions.typer.Srav;
import com.mips.instructions.typer.Srl;
import com.mips.instructions.typer.Srlv;
import com.mips.instructions.typer.Sub;
import com.mips.instructions.typer.Subu;
import com.mips.instructions.typer.Syscall;
import com.mips.instructions.typer.Xor;
import com.mips.util.Util;

/**
 * This Class represents the instruction concept, the instruction type will
 * extend of this one
 */
public final class InstructionManager {

	/** The binary represented as a char array. */
	private char[] binary;

	/** The instruction managed. */
	private Instruction instruction;

	/**
	 * Instantiates a new instruction.
	 *
	 * @param binary
	 *            the char array representation of the 32 bit binary
	 */
	public InstructionManager(char[] binary) {
		if (binary.length != 32)
			throw new FormatNotRecognized("This mips only accepts 32bits instructions");
		this.binary = binary;
		this.instruction = getInstructionType();
	}

	protected InstructionManager() {

	}

	/**
	 * Gets the instruction type, which can be "R", "I" or "J" at first, and
	 * then, going deeper in subclasses of this one, it will be represented as
	 * an instruction type like "addu".
	 *
	 * @return the instruction type
	 */
	public Instruction getInstructionType() {
		Instruction result = new Instruction();
		if (isTypeR()) {
			result = findInstructionTypeR();
		} else if (isTypeI()) {
			result = findInstructionTypeI();
		} else if (isTypeJ()) {
			result = findInstructionTypeJ();
		}
		return result;
	}

	/**
	 * Find the correspondent assembly instruction for type R.
	 *
	 * @return the instruction containing the assembly correspondent
	 */
	private Instruction findInstructionTypeR() {
		InstructionR result = new InstructionR(binary);
		char[] fn = result.getFn();
		String function = Util.concatChar(fn);

		switch (function) {

		/*
		 * SRL 2 SLLV 4 SRLV 6 SRAV 7 SYSCALL 12
		 */

		case "000000":
			result = new Sll(binary);
			break;

		case "000010":
			result = new Srl(binary);
			break;

		case "000011":
			result = new Sra(binary);
			break;

		case "000100": // TODO N�o sei se t� funcionando direito por conta do
						// Rs[4:0]
			result = new Sllv(binary);
			break;

		case "000110": // TODO N�o sei se t� funcionando direito por conta do
						// Rs[4:0]
			result = new Srlv(binary);
			break;

		case "000111": // TODO N�o sei se t� funcionando direito por conta do
						// Rs[4:0]
			result = new Srav(binary);
			break;

		case "001000":
			result = new Jr(binary);
			break;

		case "001100":
			result = new Syscall(binary);
			break;

		case "010000":
			result = new Mfhi(binary);
			break;

		case "010010":
			result = new Mflo(binary);
			break;

		case "011000":
			result = new Mult(binary);
			break;

		case "011001":
			result = new Multu(binary);
			break;

		case "011010":
			result = new Div(binary);
			break;

		case "011011":
			result = new Divu(binary);
			break;

		case "100000":
			result = new Add(binary);
			break;

		case "100001":
			result = new Addu(binary);
			break;

		case "100010":
			result = new Sub(binary);
			break;

		case "100011":
			result = new Subu(binary);
			break;

		case "100100":
			result = new And(binary);
			break;

		case "100101":
			result = new Or(binary);
			break;

		case "100110":
			result = new Xor(binary);
			break;

		case "100111":
			result = new Nor(binary);
			break;

		case "101010":
			result = new Slt(binary);
			break;
			
		case "101011":
			result = new Sltu(binary);
			break;

		default:

			break;
		}
		return result;
	}

	/**
	 * Find the correspondent assembly instruction for type I.
	 *
	 * @return the instruction containing the assembly correspondent
	 */
	private Instruction findInstructionTypeI() {
		InstructionI result = new InstructionI(binary);
		char[] fn = result.getOp();
		String function = Util.concatChar(fn);

		switch (function) {

		case "001011":
			result = new Sltiu(binary);
			break;

		case "001111":
			result = new Lui(binary);
			break;

		case "001000":
			result = new Addi(binary);
			break;

		case "001010":
			result = new Slti(binary);
			break;

		case "001100":
			result = new Andi(binary);
			break;

		case "001101":
			result = new Ori(binary);
			break;

		case "001110":
			result = new Xori(binary);
			break;

		case "001001":
			result = new Addiu(binary);
			break;

		case "100011":
			result = new Lw(binary);
			break;

		case "101011":
			result = new Sw(binary);
			break;

		case "100000":
			result = new Lb(binary);
			break;

		case "100100":
			result = new Lbu(binary);
			break;

		case "101000":
			result = new Sb(binary);
			break;

		case "000001":
			result = new Bltz(binary);
			break;

		case "000100":
			result = new Beq(binary);
			break;

		case "000101":
			result = new Bne(binary);
			break;

		default:

			break;
		}
		return result;
	}

	/**
	 * Find the correspondent assembly instruction for type J.
	 *
	 * @return the instruction containing the assembly correspondent
	 */
	private Instruction findInstructionTypeJ() {
		InstructionJ result = new InstructionJ(binary);
		char[] fn = result.getOp();
		String function = Util.concatChar(fn);

		switch (function) {

		case "000010":
			result = new J(binary);
			break;

		case "000011":
			result = new Jal(binary);
			break;

		default:

			break;
		}
		return result;
	}

	/**
	 * Checks if it starts with 5 zeros. If so, it is type R
	 *
	 * @return true, if is type R
	 */
	private boolean isTypeR() {
		for (int i = 0; i < 6; i++) {
			if (binary[i] != '0') {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the opcode is different of X"00", X"02" and X"03". If so, it is
	 * type I
	 *
	 * @return true, if is type I
	 */
	private boolean isTypeI() {
		boolean result = false;
		char[] opcode = Arrays.copyOfRange(binary, 0, 6);
		int inOpc = Util.binToDec(opcode);
		if (inOpc != 0 && inOpc != 2 && inOpc != 3)
			result = true;
		return result;
	}

	/**
	 * Checks if the opcode is X"02" or X"03". If so, it is type J
	 *
	 * @return true, if is type J
	 */
	private boolean isTypeJ() {
		boolean result = false;
		char[] opcode = Arrays.copyOfRange(binary, 0, 6);
		int inOpc = Util.binToDec(opcode);
		if (inOpc == 2 || inOpc == 3)
			result = true;
		return result;
	}

	/**
	 * Gets the binary char array.
	 *
	 * @return the binary array
	 */
	public char[] getBinary() {
		return binary;
	}

	public String getAsAssembly() {
		return this.instruction.getAsAssembly();
	}
}
