package com.mips.instructions;

import java.util.Arrays;

import com.mips.exceptions.FormatNotRecognized;

/**
 * This Class is the representation of the instructions type R, it extends
 * Instruction class.
 */
public class InstructionR extends Instruction {

	/** The Opcode bits, represented as char array. */
	private char[] op;

	/** The Register S bits, represented as char array. */
	private char[] rs;

	/** The Register t bits, represented as char array. */
	private char[] rt;

	/** The Register d bits, represented as char array. */
	private char[] rd;

	/** The Shamit bits, represented as char array. */
	private char[] sh;

	/** The Function bits, represented as char array. */
	private char[] fn;

	/**
	 * Instantiates a new instruction R.
	 *
	 * @param bin
	 *            the binary array
	 */
	public InstructionR(char[] bin) {
		setOp(Arrays.copyOfRange(bin, 0, 6));
		setRs(Arrays.copyOfRange(bin, 6, 11));
		setRt(Arrays.copyOfRange(bin, 11, 16));
		setRd(Arrays.copyOfRange(bin, 16, 21));
		setSh(Arrays.copyOfRange(bin, 21, 26));
		setFn(Arrays.copyOfRange(bin, 26, 32));
	}

	/**
	 * Gets the opcode bits.
	 *
	 * @return the opcode bits as char array
	 */
	public char[] getOp() {
		return op;
	}

	/**
	 * Gets the register s bits.
	 *
	 * @return the resgister s bits as char array
	 */
	public char[] getRs() {
		return rs;
	}

	/**
	 * Gets the register t bits.
	 *
	 * @return the register t bits as char array
	 */
	public char[] getRt() {
		return rt;
	}

	/**
	 * Gets the register d bits.
	 *
	 * @return the register d bits as char array
	 */
	public char[] getRd() {
		return rd;
	}

	/**
	 * Gets the Shamit bits.
	 *
	 * @return the shamit bits as char array
	 */
	public char[] getSh() {
		return sh;
	}

	/**
	 * Gets the function bits.
	 *
	 * @return the function bits as char array
	 */
	public char[] getFn() {
		return fn;
	}

	/**
	 * Sets the opcode.
	 *
	 * @param op
	 *            the new opcode bits as char array
	 */
	public void setOp(char[] op) {
		if (op.length != 6 || !allZeros(op))
			throw new FormatNotRecognized("The Opcode of this instruction is not recognized");
		this.op = op;
	}

	/**
	 * Sets the register s bits.
	 *
	 * @param rs
	 *            the new register s bits as char array
	 */
	public void setRs(char[] rs) {
		if (rs.length != 5)
			throw new FormatNotRecognized("The RS of this instruction is not recognized");
		this.rs = rs;
	}

	/**
	 * Sets the register t bits.
	 *
	 * @param rt
	 *            the new register t bits as char array
	 */
	public void setRt(char[] rt) {
		if (rt.length != 5)
			throw new FormatNotRecognized("The Rt of this instruction is not recognized");
		this.rt = rt;
	}

	/**
	 * Sets the register d bits.
	 *
	 * @param rd
	 *            the new register d bits as char array
	 */
	public void setRd(char[] rd) {
		if (rd.length != 5)
			throw new FormatNotRecognized("The Rd of this instruction is not recognized");
		this.rd = rd;
	}

	/**
	 * Sets the Shamit bits.
	 *
	 * @param sh
	 *            the new Shamit bits as char array
	 */
	public void setSh(char[] sh) {
		if (sh.length != 5)
			throw new FormatNotRecognized("The Shamt of this instruction is not recognized");
		this.sh = sh;
	}

	/**
	 * Sets the function bits.
	 *
	 * @param fn
	 *            the new Function bits as char array
	 */
	public void setFn(char[] fn) {
		if (fn.length != 6)
			throw new FormatNotRecognized("The Function of this instruction is not recognized");
		this.fn = fn;
	}

	/**
	 * Checks if it has only zeros. It is used to check if the Opcode is
	 * restrictively 0.
	 *
	 * @param op
	 *            the opcode bits.
	 * @return true, if has only zeros.
	 */
	private boolean allZeros(char[] op) {
		for (char c : op) {
			if (c != '0')
				return false;
		}
		return true;
	}
}
