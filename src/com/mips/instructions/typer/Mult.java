package com.mips.instructions.typer;

import java.math.BigInteger;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction Mult.
 */
public final class Mult extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "mult $%d, $%d";

	/** The arg1 **/
	private int arg1;

	/** The arg2 **/
	private int arg2;

	/**
	 * Instantiates a new mult instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Mult(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRs());
		arg2 = Util.binToDec(super.getRt());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2);

	}

	@Override
	public void performAction() {	
		RegisterBank bank = RegisterBank.getInstance();
	
		int value1 = bank.getRegValue(arg1);
		int value2 = bank.getRegValue(arg2);
		BigInteger result = BigInteger.valueOf(value1).multiply(BigInteger.valueOf(value2));
		BigInteger hi = result.shiftLeft(32);
		BigInteger lo = result.shiftRight(32);
		bank.setMfhi(hi.intValue());
		bank.setMflo(lo.intValue());
	}

}