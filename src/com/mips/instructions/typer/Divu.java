package com.mips.instructions.typer;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction divu.
 */
public final class Divu extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "divu $%d, $%d";
	
	/** The arg1 **/
	private int arg1;

	/** The arg2 **/
	private int arg2;

	/**
	 * Instantiates a new divu instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Divu(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRs());
		arg2 = Util.binToDec(super.getRt());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2);

	}
	
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int value1 = bank.getRegValue(arg1);
		int value2 = bank.getRegValue(arg2);
		int mfhi;
		int mflo;
		if (value2 > 0) {
			mfhi = value1 % value2;
			mflo = value1 / value2;
		} else {
			mfhi = 0;
			mflo = 0;

		}
		bank.setMfhi(mfhi);
		bank.setMflo(mflo);
	}

}