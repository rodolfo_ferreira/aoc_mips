package com.mips.instructions.typer;

import com.mips.instructions.InstructionR;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction Add.
 */
public final class Syscall extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "syscall";

	/**
	 * Instantiates a new addu instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Syscall(char[] bin) {
		super(bin);
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE);

	}
	
	@Override
	public void performAction() {
		//TODO
		// ????
	}

}