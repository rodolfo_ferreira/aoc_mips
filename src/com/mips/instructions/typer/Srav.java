package com.mips.instructions.typer;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction Srav.
 */
public final class Srav extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "srav $%d, $%d, $%d";
	
	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new srav instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Srav(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRd());
		arg2 = Util.binToDec(super.getRt());
		arg3 = Util.binToDec(super.getRs());
		performAction();


	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		// shift right arithmetic
		RegisterBank bank = RegisterBank.getInstance();
		int regRt = bank.getRegValue(arg2);
		int shift = bank.getRegValue(arg3);
		char[] regRtBin = Util.decToBin(regRt);
		
		for(int i=0; i < shift; i++){
			regRtBin = rShift(regRtBin);
		}
				
		int value = Util.binToDec(regRtBin);
		bank.setRegValue(arg1, value);
	}
	
	public char[] rShift(char[] bits){
		char[] saida = new char[bits.length];
		for (int i = 1; i < saida.length; i++) {
			if(i==1){
				saida[i-1] = bits[i-1];
				saida[i] = bits[i-1];
			}else{
				saida[i] = bits[i-1];
			}
		}
		return saida;
	}

}