package com.mips.instructions.typer;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction Srl.
 */
public final class Srl extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "srl $%d, $%d, %d";
	
	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new srl instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Srl(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRd());
		arg2 = Util.binToDec(super.getRt());
		arg3 = Util.binToDec(super.getSh());
		performAction();


	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		// shift right logical
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg2);
				
		int value = (int) (regAdd1 / Math.pow(2, arg3));
		bank.setRegValue(arg1, value);


	}

}