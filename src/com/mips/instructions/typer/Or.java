package com.mips.instructions.typer;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction or.
 */
public final class Or extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "or $%d, $%d, $%d";
	
	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new or instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Or(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRd());
		arg2 = Util.binToDec(super.getRs());
		arg3 = Util.binToDec(super.getRt());
		performAction();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg2);
		int regAdd2 = bank.getRegValue(arg3);
		
		//Converte Dec to Bin
		char[] regAdd1Bin = Util.decToBin(regAdd1);
		char[] regAdd2Bin = Util.decToBin(regAdd2);
		char[] valueBin = new char[32];
		int value = 0;
		
		// Faz OR l�gico bit a bit
		for (int i = 0; i < regAdd1Bin.length; i++) {
			if(regAdd1Bin[i]=='1' || regAdd2Bin[i]=='1'){
				valueBin[i] = '1';
			}else{
				valueBin[i] = '0';
			}
		}
		value = Util.binToDec(valueBin);
		bank.setRegValue(arg1, value);
	}

}