package com.mips.instructions.typer;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionR;
import com.mips.util.Util;

// TODO: Auto-generated Javadoc
/**
 * The Class representation of the instruction Mfhi.
 */
public final class Mfhi extends InstructionR {

	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "mfhi $%d";
	
	/** The arg1 **/
	public int arg1;

	/**
	 * Instantiates a new mfhi instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Mfhi(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRd());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1);

	}
	
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		bank.setRegValue(arg1, bank.getMfhi());
	}

}