package com.mips.instructions.typej;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionJ;
import com.mips.util.Util;

public class J extends InstructionJ {
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "j %d";

	/** the arg1 **/
	private int arg1;

	/**
	 * Instantiates a new j instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public J(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getTg());
		performAction();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1);

	}

	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		bank.setPc(arg1 * 4);
	}

}
