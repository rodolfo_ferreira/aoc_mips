package com.mips.instructions.typej;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionJ;
import com.mips.util.Util;

public class Jal extends InstructionJ {
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "jal %d";

	/** the arg1 **/
	private int arg1;

	/**
	 * Instantiates a new jal instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Jal(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getTg());
		performAction();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1);

	}

	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		bank.setRegValue(31, bank.getPc());
		bank.setPc(arg1 * 4);
	}

}
