package com.mips.instructions.typei;

import java.util.Arrays;

import com.mips.Memory;
import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Sb extends InstructionI{
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "sb $%d, %d($%d)";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;
	
	/**
	 * Instantiates a new sb instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Sb(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDec(super.getI());
		arg3 = Util.binToDec(super.getRs());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		Memory memory = Memory.getInstance();
		RegisterBank registerBank = RegisterBank.getInstance();
		int address = registerBank.getRegValue(arg3) + arg2;
		int value = registerBank.getRegValue(arg1);
		char[] bin = Util.decToBinComp2(value);
		bin = Arrays.copyOfRange(bin, 24, 32);
		memory.put(address, bin);

	}


}

