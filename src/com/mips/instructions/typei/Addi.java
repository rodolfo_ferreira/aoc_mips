package com.mips.instructions.typei;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Addi extends InstructionI{
	
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "addi $%d, $%d, %d";
	
	/** The argument 1. */
	private int arg1;

	/** The argument 2. */
	private int arg2;

	/** The argument 3. */
	private int arg3;

	/**
	 * Instantiates a new addi instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Addi(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDec(super.getRs());
		arg3 = Util.binToDecComp2(super.getI());
		performAction();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg2);
		int value = regAdd1 + arg3;
		bank.setRegValue(arg1, value);

	}

}
