package com.mips.instructions.typei;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Lui extends InstructionI{
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "lui $%d, %d";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;
	
	/**
	 * Instantiates a new lui instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Lui(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRs());
		arg2 = Util.binToDec(super.getRt());
		arg3 = Util.binToDecComp2(super.getI());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {

		int rt = Util.binToDec(super.getRt());
		int i = Util.binToDec(super.getI());
		return String.format(PATTERN_MESSAGE, rt, i);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg3);
		int value = (int) (regAdd1 * Math.pow(2, 16));
		bank.setRegValue(arg2, value);

	}

}
