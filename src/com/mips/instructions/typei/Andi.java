package com.mips.instructions.typei;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Andi extends InstructionI{
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "andi $%d, $%d, %d";
	
	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new andi instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Andi(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDec(super.getRs());
		arg3 = Util.binToDecComp2(super.getI());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg2);
		int regAdd2 = arg3;
		
		//Converte Dec to Bin
		char[] regAdd1Bin = Util.decToBin(regAdd1);
		char[] regAdd2Bin = Util.decToBin(regAdd2);
		char[] valueBin = new char[32];
		int value = 0;
		
		// Faz ANDi l�gico bit a bit
		for (int i = 0; i < regAdd1Bin.length; i++) {
			if(regAdd1Bin[i]=='1' & regAdd2Bin[i]=='1'){
				valueBin[i] = '1';
			}else{
				valueBin[i] = '0';
			}
		}
		value = Util.binToDec(valueBin);
		bank.setRegValue(arg1, value);

	}

}
