package com.mips.instructions.typei;

import com.mips.Memory;
import com.mips.RegisterBank;
import com.mips.exceptions.FormatNotRecognized;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Lw extends InstructionI {
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "lw $%d, %d($%d)";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new lw instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Lw(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDec(super.getI());
		arg3 = Util.binToDec(super.getRs());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		Memory memory = Memory.getInstance();
		RegisterBank registerBank = RegisterBank.getInstance();
		if (arg2 % 4 != 0)
			throw new FormatNotRecognized("The offset value is not recognizeds");
		Integer address = (registerBank.getRegValue(arg3) + arg2) + 3;
		char[] binValue = new char[32];
		for (int i = 0; i < 4; i++) {
			char[] bin = memory.get(address - i);
			for (int j = 0; j < 8; j++) {
				binValue[i * 8 + j] = bin[j];
			}
		}
		int value = Util.binToDecComp2(binValue);
		registerBank.setRegValue(arg1, value);

	}

}
