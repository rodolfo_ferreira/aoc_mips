package com.mips.instructions.typei;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Beq extends InstructionI {
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "beq $%d, $%d, %d";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new beq instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Beq(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRs());
		arg2 = Util.binToDec(super.getRt());
		arg3 = Util.binToDecComp2(super.getI());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		if (bank.getRegValue(arg1) == bank.getRegValue(arg2))
			bank.setPc(bank.getPc() + (arg3 * 4));

	}

}
