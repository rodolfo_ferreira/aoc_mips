package com.mips.instructions.typei;

import java.util.Arrays;

import com.mips.Memory;
import com.mips.RegisterBank;
import com.mips.exceptions.FormatNotRecognized;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Sw extends InstructionI {
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "sw $%d, %d($%d)";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;

	/**
	 * Instantiates a new sw instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Sw(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDecComp2(super.getI());
		arg3 = Util.binToDec(super.getRs());
		performAction();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		Memory memoria = Memory.getInstance();
		RegisterBank registerBank = RegisterBank.getInstance();
		if (arg2 % 4 != 0)
			throw new FormatNotRecognized("The offset value is not recognizeds");
		int value = registerBank.getRegValue(arg1);
		int address = (registerBank.getRegValue(arg3) + arg2) + 3;
		char[] bin = Util.decToBinComp2(value);
		for (int i = 0; i < 4; i++) {
			char[] thisArray = Arrays.copyOfRange(bin, i * 8, i * 8 + 8);
			memoria.put(address, thisArray);
			address--;
		}

	}

}
