package com.mips.instructions.typei;

import com.mips.RegisterBank;
import com.mips.instructions.InstructionI;
import com.mips.util.Util;

public class Slti extends InstructionI{
	/** The Constant PATTERN_MESSAGE to build a string for the assembly code. */
	public final static String PATTERN_MESSAGE = "slti $%d, $%d, %d";

	/** The arg 1. */
	private int arg1;

	/** The arg 2. */
	private int arg2;

	/** The arg 3. */
	private int arg3;
	
	/**
	 * Instantiates a new slti instruction.
	 *
	 * @param bin
	 *            the binary
	 */
	public Slti(char[] bin) {
		super(bin);
		arg1 = Util.binToDec(super.getRt());
		arg2 = Util.binToDec(super.getRs());
		arg3 = Util.binToDecComp2(super.getI());
		performAction();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.InstructionR#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return String.format(PATTERN_MESSAGE, arg1, arg2, arg3);

	}
	

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mips.instructions.Instruction#performAction()
	 */
	@Override
	public void performAction() {
		RegisterBank bank = RegisterBank.getInstance();
		int regAdd1 = bank.getRegValue(arg2);
		if(regAdd1 < arg3){
			bank.setRegValue(arg1, 1);
		}else{
			bank.setRegValue(arg1, 0);
		}
	
	}

}
