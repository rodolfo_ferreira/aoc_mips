package com.mips.instructions;

import java.util.Arrays;

import com.mips.exceptions.FormatNotRecognized;

public class InstructionJ extends Instruction{
	
	/** The Opcode bits, represented as char array. */
	private char[] op;
	
	/** The Target bits, represented as char array. */
	private char[] tg;
	
	/**
	 * Instantiates a new instruction J.
	 *
	 * @param bin
	 *            the binary array
	 */
	public InstructionJ(char[] bin) {
		setOp(Arrays.copyOfRange(bin, 0, 6));
		setTg(Arrays.copyOfRange(bin, 6, 32));
	}
	
	/* (non-Javadoc)
	 * @see com.mips.instructions.Instruction#getAsAssembly()
	 */
	@Override
	public String getAsAssembly() {
		return "COULDN'T RECOGNIZE THIS INSTRUCTION J TYPE";
	}

	/**
	 * Gets the opcode bits.
	 *
	 * @return the opcode bits as char array
	 */
	public char[] getOp() {
		return op;
	}

	/**
	 * Gets the target bits.
	 *
	 * @return the target bits as char array
	 */
	public char[] getTg() {
		return tg;
	}
	
	/**
	 * Sets the opcode.
	 *
	 * @param op
	 *            the new opcode bits as char array
	 */
	public void setOp(char[] op) {
		if (op.length != 6)
			throw new FormatNotRecognized("The Opcode of this instruction is not recognized");
		this.op = op;
	}

	/**
	 * Sets the target.
	 *
	 * @param tg
	 *            the new target bits as char array
	 */
	public void setTg(char[] tg) {
		if (tg.length != 26)
			throw new FormatNotRecognized("The Target of this instruction is not recognized");
		this.tg = tg;
	}

	
}
