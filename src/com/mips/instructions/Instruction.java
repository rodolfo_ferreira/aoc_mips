package com.mips.instructions;

/**
 * The Class which represents a Instruction.
 */
public class Instruction {

	/**
	 * Gets the assembly instruction.
	 *
	 * @return the as assembly
	 */
	public String getAsAssembly() {
		String result = "THE INSTRUCTION IS NOT RECOGNIZED";
		return result;
	}

	/**
	 * Performs the instruction action in the register bank 
	 * 
	 */
	public void performAction(){
		System.err.println("Some instruction was not recognized and has not performed the action");
	}

}
