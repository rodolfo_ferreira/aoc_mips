package com.mips.instructions;

import java.util.Arrays;

import com.mips.exceptions.FormatNotRecognized;

public class InstructionI extends Instruction{
	
	/** The Opcode bits, represented as char array. */
	private char[] op;

	/** The Register S bits, represented as char array. */
	private char[] rs;

	/** The Register t bits, represented as char array. */
	private char[] rt;

	/** The Immediate bits, represented as char array. */
	private char[] i;
	
	/**
	 * Instantiates a new instruction I.
	 *
	 * @param bin
	 *            the binary array
	 */
	public InstructionI(char[] bin) {
		setOp(Arrays.copyOfRange(bin, 0, 6));
		setRs(Arrays.copyOfRange(bin, 6, 11));
		setRt(Arrays.copyOfRange(bin, 11, 16));
		setI(Arrays.copyOfRange(bin, 16, 32));
	}
	
	/**
	 * Gets the opcode bits.
	 *
	 * @return the opcode bits as char array
	 */
	public char[] getOp() {
		return op;
	}

	/**
	 * Gets the register s bits.
	 *
	 * @return the resgister s bits as char array
	 */
	public char[] getRs() {
		return rs;
	}

	/**
	 * Gets the register t bits.
	 *
	 * @return the register t bits as char array
	 */
	public char[] getRt() {
		return rt;
	}
	
	/**
	 * Gets the Immediate bits.
	 *
	 * @return the Immediate bits as char array
	 */
	public char[] getI() {
		return i;
	}

	/**
	 * Sets the opcode.
	 *
	 * @param op
	 *            the new opcode bits as char array
	 */
	public void setOp(char[] op) {
		if (op.length != 6)
			throw new FormatNotRecognized("The Opcode of this instruction is not recognized");
		this.op = op;
	}

	/**
	 * Sets the register s bits.
	 *
	 * @param rs
	 *            the new register s bits as char array
	 */
	public void setRs(char[] rs) {
		if (rs.length != 5)
			throw new FormatNotRecognized("The RS of this instruction is not recognized");
		this.rs = rs;
	}

	/**
	 * Sets the register t bits.
	 *
	 * @param rt
	 *            the new register t bits as char array
	 */
	public void setRt(char[] rt) {
		if (rt.length != 5)
			throw new FormatNotRecognized("The Rt of this instruction is not recognized");
		this.rt = rt;
	}

	/**
	 * Sets Immediate d bits.
	 *
	 * @param i
	 *            the new Immediate bits as char array
	 */
	public void setI(char[] i) {
		if (i.length != 16)
			throw new FormatNotRecognized("The Immediate of this instruction is not recognized");
		this.i = i;
	}

}
