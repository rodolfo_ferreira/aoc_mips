Bitbucket Project: https://bitbucket.org/rodolfo_ferreira/aoc_mips

Team:
	Caio Silva dos Santos
	Juliana Ferreira dos Santos
	Renan Victor Maciel de Oliveira Luna
	Rodolfo Andre Barbosa Ferreira
	


How to run the code:

ATTENTION! THE INSTRUCTIONS HAVE TO BE SEPARATED WITH BREAKLINE "\n" IN THE FILE.

1. In the main method declare a String with the path of the file to be converted
2. Instantiate the Mips object then use the method "load" to load the input file and set the output file in method "setOutput".
3. Call the method "execute" from object Mips.
4. Get the assembly code with the method "getAssemblyCode()" from object Mips.
5. The default path to input is the Project Folder>input>entrada.txt
6. The default path to output is the Project Folder>output>saida.txt
7. To use the binary just use the Javac command and pass as the first argument the Input Path String, and second argument the 
	Output Path String.

There is an Example in the Class Example in package "com.mips" for futher doubts.